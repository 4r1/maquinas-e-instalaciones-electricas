\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivo}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Introducci\IeC {\'o}n}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Consignas}{1}{section.1.3}
\contentsline {chapter}{\numberline {2}Desarrollo}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Problemas 1 al 6}{5}{section.2.1}
\contentsline {subsection}{Entradas y salidas}{5}{section*.2}
\contentsline {subsection}{Diagrama}{6}{section*.3}
\contentsline {section}{\numberline {2.2}Problema 7}{7}{section.2.2}
\contentsline {subsection}{Demostraci\IeC {\'o}n mediante circuitos equivalentes}{7}{section*.4}
\contentsline {subsection}{Demostraci\IeC {\'o}n en forma anal\IeC {\'\i }tica}{7}{section*.5}
\contentsline {section}{\numberline {2.3}Problema 8}{8}{section.2.3}
\contentsline {subsection}{Entradas y salidas}{8}{section*.6}
\contentsline {subsection}{Diagrama}{8}{section*.7}
\contentsline {section}{\numberline {2.4}Problema 9}{9}{section.2.4}
\contentsline {subsection}{Entradas y salidas}{9}{section*.8}
\contentsline {subsection}{Diagrama}{9}{section*.9}
\contentsline {section}{\numberline {2.5}Problema 10}{9}{section.2.5}
\contentsline {subsection}{Entradas y salidas}{9}{section*.10}
\contentsline {subsection}{Diagrama}{9}{section*.11}
\contentsline {section}{\numberline {2.6}Problema 11}{10}{section.2.6}
\contentsline {subsection}{Entradas y salidas}{10}{section*.12}
\contentsline {subsection}{Diagrama}{10}{section*.13}
\contentsline {section}{\numberline {2.7}Problema 12}{10}{section.2.7}
\contentsline {subsection}{Entradas y salidas}{10}{section*.14}
\contentsline {subsection}{Diagrama}{10}{section*.15}
\contentsline {section}{\numberline {2.8}Problema 13}{10}{section.2.8}
\contentsline {subsection}{Entradas y salidas}{10}{section*.16}
\contentsline {subsection}{Diagrama}{11}{section*.17}
\contentsline {section}{\numberline {2.9}Problema 14}{11}{section.2.9}
\contentsline {subsection}{Entradas y salidas}{11}{section*.18}
\contentsline {subsection}{Diagrama}{12}{section*.19}
\contentsline {section}{\numberline {2.10}Problema 15}{12}{section.2.10}
\contentsline {subsection}{Entradas y salidas}{12}{section*.20}
\contentsline {subsection}{Diagrama}{13}{section*.21}
\contentsline {section}{\numberline {2.11}Problema 16}{14}{section.2.11}
\contentsline {subsection}{Entradas y salidas}{14}{section*.22}
\contentsline {subsection}{Diagrama}{14}{section*.23}
\contentsline {section}{\numberline {2.12}Problema 17}{15}{section.2.12}
\contentsline {subsection}{Entradas y salidas}{15}{section*.24}
\contentsline {subsection}{Diagrama}{16}{section*.25}
\contentsline {chapter}{\numberline {3}Conclusi\IeC {\'o}n}{17}{chapter.3}
