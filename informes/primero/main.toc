\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivo}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}M\IeC {\'e}todos de Desarrollo}{2}{section.1.2}
\contentsline {subsection}{Microchip PIC / Atmel AVR}{2}{section*.2}
\contentsline {subsection}{OpenPLC + Raspberry PI / Arduino}{2}{section*.3}
\contentsline {section}{\numberline {1.3}Requisitos del PLC}{2}{section.1.3}
\contentsline {chapter}{\numberline {2}Desarrollo}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Dise\IeC {\~n}o}{3}{section.2.1}
\contentsline {subsection}{Circuito de entrada}{3}{section*.4}
\contentsline {subsection}{Circuito de salida}{3}{section*.6}
\contentsline {subsection}{Limitaciones}{4}{section*.7}
\contentsline {section}{\numberline {2.2}Software}{4}{section.2.2}
\contentsline {section}{\numberline {2.3}Esquem\IeC {\'a}ticos}{5}{section.2.3}
\contentsline {chapter}{\numberline {3}Conclusiones}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Inconvenientes}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}Conclusion Final}{7}{section.3.2}
