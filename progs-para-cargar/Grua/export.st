PROGRAM program0
  VAR
    Pa AT %IX0.0 : BOOL;
    Pb AT %IX0.1 : BOOL;
    S1 AT %IX0.2 : BOOL;
    S2 AT %IX0.3 : BOOL;
    S3 AT %IX0.4 : BOOL;
    S4 AT %IX0.5 : BOOL;
    Subir AT %QX0.0 : BOOL;
    Bajar AT %QX0.1 : BOOL;
    Der AT %QX0.2 : BOOL;
    Izq AT %QX0.3 : BOOL;
    Iman AT %QX0.6 : BOOL;
    M0 AT %M0 : BOOL;
    M1 AT %M1 : BOOL;
    M2 AT %M2 : BOOL;
    M3 AT %M3 : BOOL;
    M4 AT %M4 : BOOL;
    M5 AT %M5 : BOOL;
    M6 AT %M6 : BOOL;
    M7 AT %M7 : BOOL;
  END_VAR

  M0 := NOT(M6) AND (M0 OR Pa);
  M1 := NOT(S2) AND (S3 AND S1 AND M0 OR M1);
  M2 := NOT(S4) AND (M0 AND S3 AND S2 OR M2);
  M3 := NOT(S1) AND (M0 AND S4 AND S2 OR M3);
  M4 := NOT(M7) AND (M4 OR Pb);
  M1 := NOT(S2) AND (S4 AND S1 AND M4 OR M1);
  M5 := NOT(S3) AND (M4 AND S4 AND S2 OR M5);
  M3 := NOT(S1) AND (M4 AND S3 AND S2 OR M3);
  Subir := M1;
  Der := M2;
  Bajar := M3;
  Izq := M5;
  M6 := S4 AND S1;
  Iman := M0;
  M7 := S3 AND S1;
END_PROGRAM


CONFIGURATION Config0

  RESOURCE Res0 ON PLC
    TASK TaskMain(INTERVAL := T#50ms,PRIORITY := 0);
    PROGRAM Inst0 WITH TaskMain : program0;
  END_RESOURCE
END_CONFIGURATION
